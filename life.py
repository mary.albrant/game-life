import numpy as np
import imageio
import matplotlib.pyplot as plt


class Cellule(object):
    """
    Class "Cellule". The cell has coordinates x and y, the number of living neighbors "neighs",
    and a parameter "alive", that determines whether it is alive or not.
    """
    def __init__(self, x, y, alive, neighs):
        self.x = int(x)
        self.y = int(y)
        self.alive = bool(alive)
        self.neighs = neighs

    def next_day(self):
        """
        This function determines whether this cell will be alive or not on the next step.
        :return: cell with new parameter "alive"
        """
        if (self.neighs == 3) and (self.alive is False):
            survive = 1
        elif (2 <= self.neighs <= 3) and (self.alive is True):
            survive = 1
        else:
            survive = 0
        return Cellule(self.x, self.y, survive, self.neighs)

    def neighbors_number(self, matrix):
        """
        This function counts how many alive neighbors has _cell_
        neighbors' coordinates:
        lb/rb -- left/right, bottom
        lc/rc -- left/right, center
        lt/rt -- left/right, top
        ...
        :return: cell with new parameter "neighs"
        """
        lb = [self.x - 1, self.y - 1]
        lc = [self.x - 1, self.y]
        lt = [self.x - 1, self.y + 1]
        tt = [self.x, self.y + 1]
        rt = [self.x + 1, self.y + 1]
        rc = [self.x + 1, self.y]
        rb = [self.x + 1, self.y - 1]
        bb = [self.x, self.y - 1]

        neighbors = 0
        if (self.x == 0) and (self.y == 0):
            for indx in [tt, rt, rc]:
                if matrix[indx[0]][indx[1]].alive is True:
                    neighbors += 1
        elif (self.x == 0) and (0 < self.y < n - 1):
            for indx in [tt, rt, rc, rb, bb]:
                if matrix[indx[0]][indx[1]].alive is True:
                    neighbors += 1
        elif (self.x == 0) and (self.y == n - 1):
            for indx in [rc, rb, bb]:
                if matrix[indx[0]][indx[1]].alive is True:
                    neighbors += 1
        elif (self.x == n - 1) and (self.y == 0):
            for indx in [tt, lt, lc]:
                if matrix[indx[0]][indx[1]].alive is True:
                    neighbors += 1
        elif (self.x == n - 1) and (0 < self.y < n - 1):
            for indx in [tt, lt, lc, lb, bb]:
                if matrix[indx[0]][indx[1]].alive is True:
                    neighbors += 1
        elif (self.x == n - 1) and (self.y == n - 1):
            for indx in [lc, lb, bb]:
                if matrix[indx[0]][indx[1]].alive is True:
                    neighbors += 1
        elif (0 < self.x < n - 1) and (self.y == n - 1):
            for indx in [lc, lb, bb, rb, rc]:
                if matrix[indx[0]][indx[1]].alive is True:
                    neighbors += 1
        elif (0 < self.x < n - 1) and (self.y == 0):
            for indx in [lc, lt, tt, rt, rc]:
                if matrix[indx[0]][indx[1]].alive is True:
                    neighbors += 1
        else:
            for indx in [lc, lb, bb, rb, rc, rt, tt, lt]:
                if matrix[indx[0]][indx[1]].alive is True:
                    neighbors += 1

        return Cellule(self.x, self.y, self.alive, neighbors)


#     initial data    #
n = 15
desk = []
for i in range(n):
    row = []
    for j in range(n):
        row.append(Cellule(i, j, 0, 0))
    desk.append(row)
desk[0][n-2] = Cellule(0, n-2, 1, 0)
desk[1][n-3] = Cellule(1, n-3, 1, 0)
desk[2][n-3] = Cellule(2, n-3, 1, 0)
desk[2][n-2] = Cellule(2, n-2, 1, 0)
desk[2][n-1] = Cellule(2, n-1, 1, 0)
#######################

image_files = []
days = range(50)
for day in days:
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    plt.vlines(range(n + 1), ymin=-1, ymax=n+1, color='tab:grey')
    plt.hlines(range(n + 1), xmin=-1, xmax=n+1, color='tab:grey')
    ax.set_xlim(-1, n+1)
    ax.set_ylim(-1, n+1)
    """ count neighbors and draw next day picture """
    for i in range(n):
        for j in range(n):
            """ counting neighbors for all cells """
            desk[i][j] = desk[i][j].neighbors_number(desk)
            """ drawing cells """
            if desk[i][j].alive is True:
                circle = plt.Circle((desk[i][j].x + 0.5, desk[i][j].y + 0.5), 0.45, color='#88ff00')
                ax.add_artist(circle)

    plt.savefig(f"image{day}.png")
    image_files.append(f"image{day}.png")

    """ updating desk """
    for i in range(n):
        for j in range(n):
            desk[i][j] = desk[i][j].next_day()

images = []
for filename in image_files:
    images.append(imageio.imread(filename))
imageio.mimsave("life.gif", images, fps=3)
